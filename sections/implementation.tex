%!TEX root = ../david-icsme2021.tex

%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%
\section{\babia} \label{sec:implementation}
%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%

\babia\footnote{\babia: \babiaurl} is a toolset for 3D data visualization in the browser. \babia is based on \aframe,\footnote{\aframe: \urltt{https://aframe.io}} an open web framework to build 3D, augmented reality (\ie AR), and VR experiences in the browser. \aframe extends \texttt{HTML} with new entities allowing to build 3D scenes as if they were \texttt{HTML} documents, using techniques common to any front-end web developer. \aframe is built on top of \tool{Three.js},\footnote{\tool{Three.js}: \urltt{https://threejs.org}} which uses the \webgl API available in all modern browsers.

\babia extends \aframe by providing components to create visualizations, simplify data retrieval, and manage data (\eg data filtering or mapping of fields to visualization features). Scenes built with \babia can be displayed on-screen, or in VR devices, including consumer-grade headsets. \figref{fig:vis_babia} shows a sample scene built with \babia. \babia is open source: Its source code is available on \tool{GitLab}\footnote{\babiarepo} and it can be installed with \tool{npm}.\footnote{\babianpm}

\begin{figure}[ht]
  \centering
  \includegraphics[width=\columnwidth, keepaspectratio]{img/babia_example.png}
  \caption{Example of a \babia Scene}
  \label{fig:vis_babia}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{\codecity-like Visualizations in \babia} \label{sec:city-metaphor}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\babia provides two visualizations based on \codecity. We present \babiaCodeCity, which reimplements the original \codecity but uses a different algorithm to layout the city, and presents the 3D visualization in a web browser (\ie instead of being a desktop \tool{Smalltalk} application). For the city layout, \babia employs a \emph{spiraling algorithm}: the first element is placed at the center of the spiral and the remaining elements spiral around it.

The algorithm is used recursively at all the levels of the software architecture, producing a layout in districts, that are composed of subdistricts, and so on, until the buildings are displayed at the deepest level. \figref{fig:babia_gltf} shows a scene depicted with \babiaCodeCity.

\begin{figure}[ht]
  \centering
  \includegraphics[width=0.95\columnwidth, keepaspectratio]{img/city_example.png}
  \caption{Example of a \babiaCodeCity Scene}
  \label{fig:babia_gltf}
\end{figure}

\babiaCodeCity scenes are interactive. The user can hover the cursor on a building to open a tooltip containing the name of the file together with the values of the metrics for the corresponding software artifact (\eg number of functions, lines of code, and Cyclomatic Complexity Number~\cite{complexity} of a file). The tooltip disappears when the cursor leaves the building, but pinned tooltips can be enabled by clicking on a building. To obtain information on a district (\ie a folder) the user can click on it. These interactions work in the same way on-screen (\ie with the mouse as cursor) and in VR (\ie with the controller of the VR headset as cursor). Like the original \codecity, \babiaCodeCity maps the values of software metrics to features in the visualization. In the current version of \babiaCodeCity, each building corresponds to a file. Its base area is proportional to the number of functions, its height corresponds to lines of code per function, and its color represents the Cyclomatic Complexity value (\ie in a blue to red scale).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\subsection{From Source Code to a 3D Scene} \label{sec:workflow}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

\babiaCodeCity can produce a scene starting from a commit in a \tool{Git} repository (\ie snapshot). A Python script (\ie~\tool{cocom\_graal2es.py}\footnote{\tool{cocom\_graal2es.py} docs: \babiaothersone}) clones the repository, checks out a given commit, computes the values of metrics for each file, and stores them in an \tool{ElasticSearch}\footnote{\tool{ElasticSearch}: \urltt{https://www.elastic.co/}} database. The script uses \graal~\cite{ref_graal} and \tool{Perceval}~\cite{ref_perceval} to retrieve and compute the values of metrics. \graal, in turn, uses other tools to compute metrics, via its \tool{CoCom} backend\footnote{\graal-\tool{CoCom} backend:\\ \indent \urltt{https://github.com/chaoss/grimoirelab-graal\#backends}}).

Once the results of the analysis are stored in the database, another Python script (\ie~\tool{get\_list.py}\footnote{\babiaotherstwo}) queries \tool{ElasticSearch} to produce a \texttt{JSON} document in the format required by \babiaCodeCity that contains all the information needed for the visualization. It is structured as follows:

\begin{lstlisting}
[
  { 
    "file_path": "aaa/bbb/ccc"
    "metric": x,
    "metric2": y,
    ...
  },
  ...
]
\end{lstlisting}

The scene to visualize this data is composed of a single \texttt{HTML} file, which uses the \texttt{JSON} document. The \texttt{HTML} file imports all the dependencies (\ie~\aframe and \babia JavaScript packages) and defines the scene by including the corresponding elements and components: \texttt{\babiapref-queryjson} to retrieve the \texttt{JSON} document, \texttt{\babiapref-treebuilder} to generate the tree-like data structure needed by \babiaCodeCity, and \texttt{\babiapref-boats} which is the actual component to generate the visualization. Each component has its own configuration, detailed in the documentation.\footnote{\babiaothersthree} The listing below shows a sample scene, including some configuration parameters:

\begin{lstlisting}[escapechar=\#]
<a-scene id="scene">
  <a-entity id="rawdata" 
    #\babiapref#-queryjson="url: data.json">
  </a-entity>
  <a-entity id="treedata" 
    #\babiapref#-treebuilder="field: field_list; 
    split_by: /; from: rawdata">
  </a-entity>
  <a-entity id="city" 
    #\babiapref#-boats="from: treedata; 
    area: metric1; height: metric2; color: metric3">
  </a-entity>
 ...
</a-scene>
\end{lstlisting}

\figref{fig:diagram} summarizes the complete workflow to produce a scene with \babiaCodeCity starting from a source code snapshot in a \tool{Git} repository.

\begin{figure}[ht]
  \centering
  \includegraphics[width=.8\columnwidth]{img/pipeline.pdf}
  \caption{\babia Workflow: From Source Code to a Scene}
  \label{fig:diagram}
\end{figure}

As we have already said, thanks to modern web technology, these scenes are fully usable in VR mode in any modern browser, including those that include VR goggles, \figref{fig:davidvr} shows an user visualizing data in a VR web scene created with \babia.

\begin{figure}[ht]
  \centering
  \includegraphics[width=\columnwidth, keepaspectratio]{img/vrdavid.jpg}
  \caption{Example of a \babia Scene in VR with Oculust Quest 2 glasses}
  \label{fig:davidvr}
\end{figure}

\tabref{fig:code_size} shows information about the code files developed for this use case, we rely on module reuse, so we are aware of the size and the modules independence, we separate frontend and backend and, in terms of visualizations, each module can be used with others or in standalone mode as the use in this approach. We have tested the size of the city (\ie number of buildings) that can be shown without noticeable glitches (at about 30-40 frames per second) in the browser. We have found that an Intel i7 machine with 8 GB of RAM, with an  Intel HD620 4K graphics card, can visualize easily cities with about 8,000 buildings both in Firefox and Chrome, on Debian, while the standard browser in Oculus Quest 2 can visualize cities of about 1,000 buildings.

\begin{table}[!htb]
  \caption{Code used basic information}
  \label{fig:code_size}
  \resizebox{\columnwidth}{!}{%
    \begin{tabular}{llll}
      \hline
      \multicolumn{1}{c}{\textbf{Code}} & \multicolumn{1}{c}{\textbf{Lines of Code}} & \multicolumn{1}{c}{\textbf{Size (in KB)}} & \multicolumn{1}{c}{\textbf{Language}} \\ \hline
      \babiapref-boats.js               & 1173                                       & 43.5                                      & JavaScript                            \\
      \babiapref-treebuilder.js         & 298                                        & 10.1                                      & JavaScript                            \\
      \babiapref-queryjson.js           & 185                                        & 4.8                                       & JavaScript                            \\
      cocom\_graal2es.py                & 100                                        & 3.25                                      & Python                                \\
      get\_list.py                      & 227                                        & 7.92                                      & Python                                \\ \hline
    \end{tabular}}
\end{table}

In addition, this special visualization of \codecity is completely integrable with the rest of visualizations and filters, so with \babia complex VR scenes can be created with more than one visualization and/or cities.